<?php
/**
 * OrderReview v0.1
 *
 *  @author    Freeman <khai603@gmail.com>
 *  @copyright 2017 Freeman
 *  @license   One Domain Licence
 */

class Order extends OrderCore
{
    public $reviewed = false;

    public function __construct($id = null, $id_lang = null)
    {
        self::$definition['fields']['reviewed'] = array('type' => self::TYPE_BOOL);
        parent::__construct($id, $id_lang);
    }

    public static function getNotReviewedOrders($date)
    {
        if(!$date || !Validate::isDate($date))
            return false;

        return Db::getInstance()->executeS('SELECT o.`id_order` FROM `'._DB_PREFIX_.'orders` o WHERE DATE(o.`date_add`) < \'' . $date . '\' AND o.`reviewed` = 0');
    }
}
