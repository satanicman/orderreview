<?php

/**
 * OrderReview v0.1
 *
 *  @author    Freeman <khai603@gmail.com>
 *  @copyright 2017 Freeman
 *  @license   One Domain Licence
 */

if (!defined('_PS_VERSION_'))
    exit;

include_once(dirname(__FILE__) . '/OrderReviewModel.php');

class OrderReview extends Module

{
    public function __construct()
    {
        $this->name = 'orderreview';
        $this->tab = 'other';
        $this->version = '0.3';
        $this->author = 'skype: not_a_free_man';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Order Products Review');
        $this->description = $this->l('Send email to the client with ask get products review');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MUMODULE_NAME'))
            $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        if(file_exists(_PS_ROOT_DIR_."/override/classes/order/Order.php")) {
            $this->_errors[] = Tools::displayError('Please delete your files Order.php in override folder, and class_index.php in cache folder');
            return false;
        }


        OrderReviewModel::addField();

        if (!parent::install()
            || !Configuration::updateValue('OR_DAY', 30)
            || !Configuration::updateValue('OR_STATE', 5)
            || !Configuration::updateValue('OR_TOKEN', md5(time())))
            return false;

        return true;
    }

    public function uninstall()
    {
        OrderReviewModel::delField();

        if (!parent::uninstall())
            return false;

        return true;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Order review'),
                    'icon' => 'icon-link'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'OR_DAY',
                        'label' => $this->l('Dey count')
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'OR_STATE',
                        'label' => $this->l('Order state'),
                        'options' => array(
                            'query' => OrderState::getOrderStates($this->context->language->id),
                            'id' => 'id_order_state',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'link',
                        'name' => 'link',
                        'value' => $this->_getLink(),
                    ),
                ),
                'submit' => array(
                    'name' => 'submit_' . $this->name,
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'OR_DAY' => (int)Configuration::get('OR_DAY'),
            'OR_STATE' => (int)Configuration::get('OR_STATE'),
        );
    }

    public function getContent()
    {
        $message = '';

        if (Tools::isSubmit('submit_' . $this->name))
            $message = $this->_saveContent();

        return $message.$this->renderForm();
    }

    private function _saveContent()
    {
        if(is_numeric(Tools::getValue('OR_DAY'))) {
            Configuration::updateValue('OR_DAY', (int)Tools::getValue('OR_DAY'));
            $message = $this->displayConfirmation($this->l("Success"));
        } else {
            $message = $this->displayError($this->l("Day count must be numerical"));
        }

        return $message;
    }

    private function _getLink()
    {
        return _PS_BASE_URL_ ."/modules/" . $this->name . "/cron.php?token=".Configuration::get('OR_TOKEN');
    }

    private function getStates()
    {
        $states = OrderState::getOrderStates($this->context->language->id);

        $result = array();
        foreach ($states as $state) {
            $result[] = array(
                'id' => $state['id_order_state'],
                'name' => $state['name']
            );
        }

        return $result;
    }
}