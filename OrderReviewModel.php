<?php

class OrderReviewModel
{
    public static function addField()
    {
        return Db::getInstance()->query('ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD `reviewed` BOOLEAN NOT NULL DEFAULT FALSE');
    }

    public static function delField()
    {
        return Db::getInstance()->query('ALTER TABLE `' . _DB_PREFIX_ . 'orders` DROP `reviewed`');
    }
}