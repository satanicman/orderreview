<?php

/**
 * OrderReview v0.1
 *
 *  @author    Freeman <khai603@gmail.com>
 *  @copyright 2017 Freeman
 *  @license   One Domain Licence
 */

if (!defined('_PS_ORDERREVIEW_DIR_')) {
    define('_PS_ORDERREVIEW_DIR_', getcwd());
}

include(_PS_ORDERREVIEW_DIR_.'/../../config/config.inc.php');
$cron = new cron();
$cron->run();

class cron
{
    public $context;

    public function run()
    {
        if(Tools::getValue('token') === Configuration::get('OR_TOKEN')) {
            $this->context = Context::getContext();
            $date = new \DateTime();
            $date->modify('- ' . (int)Configuration::get('OR_DAY') . ' days');
            $state = Configuration::get('OR_STATE');
            $orders = Order::getNotReviewedOrders($date->format('Y-m-d'));

            foreach ($orders as $order) {
                $product_var_tpl_list = array();
                $order = new Order((int)$order['id_order']);

                if(!$order->getHistory($this->context->language->id, $state))
                    continue;

                $customer = new Customer((int)$order->id_customer);
                foreach($order->getProducts() as $product) {
                    $p = new Product($product['id_product'], false, Context::getContext()->language->id);
                    $images = $p->getImages((int)Context::getContext()->language->id);
                    $image_link = '';
                    if (isset($images[0])) {
                        $image_link = $images[0];
                    }
                    foreach ($images as $k => $image) {
                        if ($image['cover']) {
                            $image_link = $image;
                            break;
                        }
                    }

                    if (!isset($image_link)) {
                        if (isset($images[0])) {
                            $image_link = $images[0];
                        } else {
                            $image_link = $this->context->language->iso_code.'-default';
                        }
                    }

                    if(is_array($image_link))
                        $image_link = Context::getContext()->link->getImageLink($p->link_rewrite, $image_link['id_image'], 'cart_default');
                    else
                        $image_link = Context::getContext()->link->getImageLink($p->link_rewrite, $image_link, 'cart_default');

                    $product_var_tpl = array(
                        'name' => $product['product_name'],
                        'link' => Context::getContext()->link->getProductLink($product),
                        'image_link' => $image_link
                    );
                    $product_var_tpl_list[] = $product_var_tpl;
                }
                $product_list_txt = '';
                $product_list_html = '';
                if (count($product_var_tpl_list) > 0) {
                    $product_list_txt = $this->getEmailTemplateContent('order_review_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list);
                    $product_list_html = $this->getEmailTemplateContent('order_review_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list);
                }

                $data = array(
                    '{firstname}' => $customer->firstname,
                    '{lastname}' => $customer->lastname,
                    '{email}' => $customer->email,
                    '{products}' => $product_list_html,
                    '{products_text}' => $product_list_txt
                );

                if (Validate::isEmail($customer->email)) {
                    $sended = Mail::Send(
                        (int)$order->id_lang,
                        'order_review',
                        Mail::l('Order review', (int)$order->id_lang),
                        $data,
                        $customer->email,
                        $customer->firstname.' '.$customer->lastname,
                        null,
                        null,
                        null,
                        null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                    );

                    if($sended) {
                        $order->reviewed = 1;
                        $order->update();
                    }
                }
            }

            die('success');
        } else {
            die('error cron');
        }
    }


    /**
     * Fetch the content of $template_name inside the folder current_theme/mails/current_iso_lang/ if found, otherwise in mails/current_iso_lang
     *
     * @param string  $template_name template name with extension
     * @param int $mail_type     Mail::TYPE_HTML or Mail::TYPE_TXT
     * @param array   $var           list send to smarty
     *
     * @return string
     */
    protected function getEmailTemplateContent($template_name, $mail_type, $var)
    {
        $email_configuration = Configuration::get('PS_MAIL_TYPE');
        if ($email_configuration != $mail_type && $email_configuration != Mail::TYPE_BOTH) {
            return '';
        }

        $theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.$this->context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;
        $default_mail_template_path = _PS_MAIL_DIR_.$this->context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;

        if (Tools::file_exists_cache($theme_template_path)) {
            $default_mail_template_path = $theme_template_path;
        }

        if (Tools::file_exists_cache($default_mail_template_path)) {
            $this->context->smarty->assign('list', $var);
            return $this->context->smarty->fetch($default_mail_template_path);
        }
        return '';
    }
}